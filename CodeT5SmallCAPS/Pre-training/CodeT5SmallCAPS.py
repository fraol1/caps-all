import importlib
import subprocess

def check_and_install_package(package_name):
    try:
        importlib.import_module(package_name)
        print(f"{package_name} is already installed.")
    except ImportError:
        print(f"{package_name} is not installed. Installing...")
        subprocess.check_call(["pip", "install", package_name])
        print(f"{package_name} has been successfully installed.")

# Example usage:
check_and_install_package("datasets")
check_and_install_package("transformers")
check_and_install_package("pytorch_lightning")
check_and_install_package("tqdm")

import os
import pprint
import argparse
from datasets import load_dataset, load_from_disk, concatenate_datasets
from transformers import RobertaTokenizer, Trainer, AutoModelForSeq2SeqLM, AdamW, get_linear_schedule_with_warmup
from torch.utils.data import DataLoader
import pytorch_lightning as pl
from pytorch_lightning import Trainer
from datasets import DatasetDict, load_dataset
import logging
import torch
from torch.utils.data import DataLoader, SequentialSampler, RandomSampler

logger = logging.getLogger(__name__)

# to properly utulize the GPU, we need to set the environment variable torch.set_float32_matmul_precision 
torch.set_float32_matmul_precision('high')

class CodeT5SmallCAPS(pl.LightningModule):
    def __init__(self, lr=2e-4, num_train_epochs=6, warmup_steps=1000, dataloader = None):
        super().__init__()
        self.model = AutoModelForSeq2SeqLM.from_pretrained("Salesforce/codet5p-220m")
        self.dataloader = dataloader 
        self.save_hyperparameters()

    def forward(self, input_ids, attention_mask, labels=None):     
        outputs = self.model(input_ids=input_ids, attention_mask=attention_mask, labels=labels)
        return outputs
    
    def common_step(self, batch, batch_idx):
        outputs = self(**batch)
        loss = outputs.loss

        return loss
      
    def training_step(self, batch, batch_idx):
        loss = self.common_step(batch, batch_idx)     
        # logs metrics for each training_step,
        # and the average across the epoch
        self.log("training_loss", loss)

        return loss

    def validation_step(self, batch, batch_idx):
        loss = self.common_step(batch, batch_idx)     
        self.log("validation_loss", loss, on_epoch=True)

        return loss

    def test_step(self, batch, batch_idx):
        loss = self.common_step(batch, batch_idx)     

        return loss

    def configure_optimizers(self):
        # create optimizer use torch.optim
        optimizer = torch.optim.AdamW(self.parameters(), lr=self.hparams.lr)
        # create learning rate scheduler
        num_train_optimization_steps = self.hparams.num_train_epochs * len(self.dataloader[0])
        lr_scheduler = {'scheduler': get_linear_schedule_with_warmup(optimizer,
                                                    num_warmup_steps=self.hparams.warmup_steps,
                                                    num_training_steps=num_train_optimization_steps),
                        'name': 'learning_rate',
                        'interval':'step',
                        'frequency': 1}
        
        return {"optimizer": optimizer, "lr_scheduler": lr_scheduler}
    
    def set_data_loaders(self, dataloader):
        self.dataloader = dataloader
        
    def train_dataloader(self):
        return self.dataloader[0]

    def val_dataloader(self):
        return self.dataloader[1]

def preprocess_examples(examples, source_name, target_name, tokenizer):
    if source_name in ['token_type', 'code_sememe']:
        prefix = "Abstract the code tokens: "
    else:
        prefix = "Identify program dependency: "
        
    source = [prefix + ex for ex in examples[target_name]]
    target = [ex for ex in examples[source_name]]

    model_inputs = tokenizer(source, max_length=args.max_source_len, padding="max_length", truncation=True)
    labels = tokenizer(target, max_length=args.max_target_len, padding="max_length", truncation=True).input_ids

    # important: we need to replace the index of the padding tokens by -100
    # such that they are not taken into account by the CrossEntropyLoss
    labels_with_ignore_index = []
    for labels_example in labels:
        labels_example = [label if label != 0 else -100 for label in labels_example]
        labels_with_ignore_index.append(labels_example)

    model_inputs["labels"] = labels_with_ignore_index

    return model_inputs

def preprocess_datasets(dataset, source_name, target_name, tokenizer, args):
    return dataset.map(lambda examples: preprocess_examples(examples, source_name, target_name, tokenizer), batched=True, batch_size = 16,
                       num_proc=64,
                       load_from_cache_file=False)

def test_output(ds, input_n, label_n):
    example = ds['train'][0]

    print("Input:", example[input_n])
    print("Label:", example[label_n])

def load_tokenize_data(args):
    if os.path.exists(args.cache_data_train_data_tt_py):
        
        train_data_tt_py = load_from_disk(args.cache_data_train_data_tt_py)
        train_data_cs_py = load_from_disk(args.cache_data_train_data_cs_py)
        train_data_pd_py = load_from_disk(args.cache_data_train_data_pd_py)
        
        train_data_tt_java = load_from_disk(args.cache_data_train_data_tt_java)
        train_data_cs_java = load_from_disk(args.cache_data_train_data_cs_java)
        train_data_pd_java = load_from_disk(args.cache_data_train_data_pd_java)
        
        print(f'  ==> Loaded {len(train_data_tt_py)} samples')
        return train_data_tt_py, train_data_cs_py, train_data_pd_py, train_data_tt_java, train_data_cs_java, train_data_pd_java

    else:
        
        tokenizer = RobertaTokenizer.from_pretrained(args.tokenizer)
        ds7, ds8, ds9 = load_dataset("CodeT5SmallCAPS/CAPS_Python"), load_dataset("LiteCoder/LT_Java_500k"), load_dataset("CodeT5SmallCAPS/CAPS_Java_Dependency")
        ds7 = load_dataset("CodeT5SmallCAPS/CAPS_Python")

        train_data_tt_py = preprocess_datasets(ds7, 'token_type', 'code', tokenizer, args)
        train_data_cs_py = preprocess_datasets(ds7, 'code_sememe', 'code', tokenizer, args)
        train_data_pd_py = preprocess_datasets(ds7, 'code_dependency', 'code', tokenizer, args)
        
        #save files to disk 
        train_data_tt_py.save_to_disk(args.cache_data_train_data_tt_py)
        train_data_cs_py.save_to_disk(args.cache_data_train_data_cs_py)
        train_data_pd_py.save_to_disk(args.cache_data_train_data_pd_py)

    
        train_data_tt_java = preprocess_datasets(ds8, 'token_type', 'code', tokenizer, args)
        train_data_cs_java = preprocess_datasets(ds8, 'code_sememe', 'code', tokenizer, args)
        train_data_pd_java = preprocess_datasets(ds9, 'code_dependency', 'code', tokenizer, args)
        
        # save files to disk 
        train_data_tt_java.save_to_disk(args.cache_data_train_data_tt_java)
        train_data_cs_java.save_to_disk(args.cache_data_train_data_cs_java)
        train_data_pd_java.save_to_disk(args.cache_data_train_data_pd_java)
        
        print(f'  ==> Saved to {args.cache_data_train_data_tt_py}')

        return train_data_tt_py, train_data_cs_py, train_data_pd_py, train_data_tt_java, train_data_cs_java, train_data_pd_java

def convert_to_loader(dataset1, dataset2, dataset3, dataset4, dataset5, dataset6, train_num=5000, val_num=2500):

    # Creating a dataset from the list of dicts
    train_datasets = [dataset1['train'].select(range(train_num)), dataset4['train'].select(range(train_num)), 
                      dataset2['train'].select(range(train_num)), dataset5['train'].select(range(train_num)),
                      dataset3['train'].select(range(train_num)), dataset6['train'].select(range(train_num))]
    
    val_datasets = [dataset1['train'].select(range(val_num)), dataset4['train'].select(range(val_num)), 
                    dataset2['train'].select(range(val_num)), dataset5['train'].select(range(val_num)),
                    dataset3['train'].select(range(val_num)), dataset6['train'].select(range(val_num))]

    # Concatenate datasets
    dataset = DatasetDict({
        'train': concatenate_datasets(train_datasets),
        'val': concatenate_datasets(val_datasets)
    })

    print(f"  ==> Loaded {len(dataset['train'])} training samples")
    print(f"  ==> Loaded {len(dataset['val'])} validation samples")

    dataset.set_format(type="torch", columns=['input_ids', 'attention_mask', 'labels'])
    
    # use a sequencial sampler to handle dataset['train']
    train_sampler = SequentialSampler(dataset['train'])
    val_sampler = SequentialSampler(dataset['val'])

    train_dataloader = DataLoader(dataset['train'], sampler=train_sampler, batch_size=4, num_workers=31)
    valid_dataloader = DataLoader(dataset['val'], sampler=val_sampler, batch_size=4, num_workers=31)

    return train_dataloader, valid_dataloader

def run_pretraining(model, train_dataloader, valid_dataloader):

    trainer = Trainer(
        max_epochs = 6,
        precision = "16-mixed",
        accelerator = "gpu",
        accumulate_grad_batches = 1,
    )

    model.set_data_loaders(dataloader=[train_dataloader, valid_dataloader])

    trainer.fit(model)

    # Save the model state for the current iteration
    model_path = f"CAPS_iteration_1"
    torch.save(model.state_dict(), model_path)

    return model

def main(args):
    argsdict = vars(args)
    print(pprint.pformat(argsdict))

    with open(os.path.join(args.save_dir, "command.txt"), 'w') as f:
        f.write(pprint.pformat(argsdict))
   
    train_data_tt_py, train_data_cs_py, train_data_pd_py, train_data_tt_java, train_data_cs_java, train_data_pd_java = load_tokenize_data(args)

    train_dataloader, valid_dataloader = convert_to_loader(train_data_tt_py, train_data_cs_py, train_data_pd_py, train_data_tt_java, train_data_cs_java, train_data_pd_java)  
    
    CAPS = CodeT5SmallCAPS() 

    run_pretraining(CAPS, train_dataloader, valid_dataloader) 
    
    model_name = "CAPS"

    # Save the model with the constructed name
    if args.train_tt:
        model_name += "_TokenType"
    if args.train_cs:
        model_name += "_CodeSememe"
    if args.train_pd:
        model_name += "_PD"
        
    model_save_path = model_name
    CAPS.model.save_pretrained(model_save_path)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Pre-training CAPS on Seq2Seq LM task with Abstract pre-training objectives.")
    parser.add_argument('-tt', '--train-tt', action='store_true', help='Train the model on Token Type Objective')
    parser.add_argument('-cs', '--train-cs', action='store_true', help='Train the model on Code Sememe Objective')
    parser.add_argument('-pd', '--train-pd', action='store_true', help='Train the model on Program Dependency Objective')
    parser.add_argument('--max-source-len', default=1024, type=int)
    parser.add_argument('--max-target-len', default=1024, type=int)
    parser.add_argument('--cache_data_train_data_tt_py', default='cache_data/CAPS_TT', type=str)
    parser.add_argument('--cache_data_train_data_cs_py', default='cache_data/CAPS_CS', type=str)
    parser.add_argument('--cache_data_train_data_pd_py', default='cache_data/CAPS_PD', type=str)
    parser.add_argument('--cache_data_train_data_tt_java', default='cache_data/CAPS_TT_java', type=str)
    parser.add_argument('--cache_data_train_data_cs_java', default='cache_data/CAPS_CS_java', type=str)
    parser.add_argument('--cache_data_train_data_pd_java', default='cache_data/CAPS_PD_java', type=str)
    parser.add_argument('--tokenizer', default='Salesforce/codet5p-770m', type=str)
    parser.add_argument('--epochs', default=1, type=int)
    parser.add_argument('--lr', default=5e-5, type=float)
    parser.add_argument('--lr-warmup-steps', default=200, type=int)
    parser.add_argument('--batch-size-per-replica', default=8, type=int)
    parser.add_argument('--grad-acc-steps', default=4, type=int)
    parser.add_argument('--local_rank', default=-1, type=int)
    parser.add_argument('--deepspeed', default=None, type=str)
    parser.add_argument('--fp16', default=False, action='store_true')
    parser.add_argument('--save-dir', default="trainingDetails/CAPS1", type=str)
    parser.add_argument('--log-freq', default=10, type=int)
    parser.add_argument('--save-freq', default=500, type=int)

    args = parser.parse_args()

    os.makedirs(args.save_dir, exist_ok=True)

    main(args)