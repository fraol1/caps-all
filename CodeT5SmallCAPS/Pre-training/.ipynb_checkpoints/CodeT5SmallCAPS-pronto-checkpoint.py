import os
import pprint
import argparse
from datasets import load_dataset, load_from_disk
from transformers import T5ForConditionalGeneration, RobertaTokenizer, TrainingArguments, Trainer, AutoModelForSeq2SeqLM
from torch.utils.data import DataLoader
from transformers import T5ForConditionalGeneration, AdamW, get_linear_schedule_with_warmup
import pytorch_lightning as pl
from torch.utils.data import ConcatDataset, DataLoader
from pytorch_lightning import Trainer
from datasets import DatasetDict, load_dataset
import logging
import torch
from torch.utils.data import DataLoader, Dataset, SequentialSampler, RandomSampler,TensorDataset
from pytorch_lightning.plugins.environments import SLURMEnvironment

logger = logging.getLogger(__name__)

class CodeT5SmallCAPS(pl.LightningModule):
    def __init__(self, lr=5e-5, num_train_epochs=2, warmup_steps=1000, dataloader = None):
        super().__init__()
        self.model = AutoModelForSeq2SeqLM.from_pretrained("Salesforce/codet5p-220m")
        self.dataloader = dataloader 
        self.save_hyperparameters()

    def forward(self, input_ids, attention_mask, labels=None):     
        outputs = self.model(input_ids=input_ids, attention_mask=attention_mask, labels=labels)
        return outputs
    
    def common_step(self, batch, batch_idx):
        outputs = self(**batch)
        loss = outputs.loss

        return loss
      
    def training_step(self, batch, batch_idx):
        loss = self.common_step(batch, batch_idx)     
        # logs metrics for each training_step,
        # and the average across the epoch
        self.log("training_loss", loss)

        return loss

    def validation_step(self, batch, batch_idx):
        loss = self.common_step(batch, batch_idx)     
        self.log("validation_loss", loss, on_epoch=True)

        return loss

    def test_step(self, batch, batch_idx):
        loss = self.common_step(batch, batch_idx)     

        return loss

    def configure_optimizers(self):
        # create optimizer
        optimizer = AdamW(self.parameters(), lr=self.hparams.lr)
        # create learning rate scheduler
        num_train_optimization_steps = self.hparams.num_train_epochs * len(self.dataloader[0])
        lr_scheduler = {'scheduler': get_linear_schedule_with_warmup(optimizer,
                                                    num_warmup_steps=self.hparams.warmup_steps,
                                                    num_training_steps=num_train_optimization_steps),
                        'name': 'learning_rate',
                        'interval':'step',
                        'frequency': 1}
        
        return {"optimizer": optimizer, "lr_scheduler": lr_scheduler}
    
    def set_data_loaders(self, dataloader):
        self.dataloader = dataloader
        
    def train_dataloader(self):
        return self.dataloader[0]

    def val_dataloader(self):
        return self.dataloader[1]

def preprocess_examples(examples, source_name, target_name, tokenizer):
    if source_name in ['token_type', 'code_sememe']:
        prefix = "Abstract the code tokens: "
    else:
        prefix = "Identify program dependency: "
        
    source = [prefix + ex for ex in examples[target_name]]
    target = [ex for ex in examples[source_name]]

    model_inputs = tokenizer(source, max_length=args.max_source_len, padding="max_length", truncation=True)
    labels = tokenizer(target, max_length=args.max_target_len, padding="max_length", truncation=True).input_ids

    # important: we need to replace the index of the padding tokens by -100
    # such that they are not taken into account by the CrossEntropyLoss
    labels_with_ignore_index = []
    for labels_example in labels:
        labels_example = [label if label != 0 else -100 for label in labels_example]
        labels_with_ignore_index.append(labels_example)

    model_inputs["labels"] = labels_with_ignore_index

    return model_inputs

def preprocess_datasets(dataset, source_name, target_name, tokenizer, args):
    return dataset.map(lambda examples: preprocess_examples(examples, source_name, target_name, tokenizer), batched=True, batch_size = 16,
                       num_proc=64,
                       load_from_cache_file=False)

def test_output(ds, input_n, label_n):
    example = ds['train'][0]

    print("Input:", example[input_n])
    print("Label:", example[label_n])

def load_tokenize_data(args):
    if os.path.exists(args.cache_data_train_data_tt_py):
        
        train_data_tt_py = load_from_disk(args.cache_data_train_data_tt_py)
        train_data_cs_py = load_from_disk(args.cache_data_train_data_cs_py)
        train_data_pd_py = load_from_disk(args.cache_data_train_data_pd_py)
        
        train_data_tt_java = load_from_disk(args.cache_data_train_data_tt_java)
        train_data_cs_java = load_from_disk(args.cache_data_train_data_cs_java)
        train_data_pd_java = load_from_disk(args.cache_data_train_data_pd_java)
        
        print(f'  ==> Loaded {len(train_data_tt_py)} samples')
        return train_data_tt_py, train_data_cs_py, train_data_pd_py, train_data_tt_java, train_data_cs_java, train_data_pd_java
        # return train_data_tt_py, train_data_cs_py, train_data_pd_py

    else:
        
        tokenizer = RobertaTokenizer.from_pretrained(args.tokenizer)
        ds7, ds8, ds9 = load_dataset("CodeT5SmallCAPS/CAPS_Python"), load_dataset("LiteCoder/LT_Java_500k"), load_dataset("CodeT5SmallCAPS/CAPS_Java_Dependency")
        ds7 = load_dataset("CodeT5SmallCAPS/CAPS_Python")

        train_data_tt_py = preprocess_datasets(ds7, 'token_type', 'code', tokenizer, args)
        train_data_cs_py = preprocess_datasets(ds7, 'code_sememe', 'code', tokenizer, args)
        train_data_pd_py = preprocess_datasets(ds7, 'code_dependency', 'code', tokenizer, args)
        
        #save files to disk 
        train_data_tt_py.save_to_disk(args.cache_data_train_data_tt_py)
        train_data_cs_py.save_to_disk(args.cache_data_train_data_cs_py)
        train_data_pd_py.save_to_disk(args.cache_data_train_data_pd_py)

    
        train_data_tt_java = preprocess_datasets(ds8, 'token_type', 'code', tokenizer, args)
        train_data_cs_java = preprocess_datasets(ds8, 'code_sememe', 'code', tokenizer, args)
        train_data_pd_java = preprocess_datasets(ds9, 'code_dependency', 'code', tokenizer, args)
        
        # save files to disk 
        train_data_tt_java.save_to_disk(args.cache_data_train_data_tt_java)
        train_data_cs_java.save_to_disk(args.cache_data_train_data_cs_java)
        train_data_pd_java.save_to_disk(args.cache_data_train_data_pd_java)
        
        print(f'  ==> Saved to {args.cache_data_train_data_tt_py}')

        return train_data_tt_py, train_data_cs_py, train_data_pd_py, train_data_tt_java, train_data_cs_java, train_data_pd_java
        # return train_data_tt_py, train_data_cs_py, train_data_pd_py

def convert_to_loader(dataset):
    
    dataset.set_format(type="torch", columns=['input_ids', 'attention_mask', 'labels'])
    
    train_sampler = RandomSampler(dataset['train'])
    train_dataloader = DataLoader(dataset['train'], sampler=train_sampler, batch_size=4)
    valid_dataloader = DataLoader(dataset['val'], batch_size=2)

    return train_dataloader, valid_dataloader

class DisabledSLURMEnvironment(SLURMEnvironment):
    def detect() -> bool:
        return False

    @staticmethod
    def _validate_srun_used() -> None:
        return

    @staticmethod
    def _validate_srun_variables() -> None:
        return
    
def run_pretraining(args, model, train_dataloader, valid_dataloader, iteration):
    print(f"Starting training iteration {iteration}")

    trainer = Trainer(
        plugins=[DisabledSLURMEnvironment(auto_requeue=False)],
        max_epochs = 1,
        precision = "16-mixed",
        max_steps = 1000,
        accelerator = "gpu",
        accumulate_grad_batches = 1,
    )
    
    # Load the model state from the previous iteration if available
    if iteration > 0:
        model_path = f"CAPS_iteration_1"
        print("Loaded saved model")
        model.load_state_dict(torch.load(model_path))

    model.set_data_loaders(dataloader=[train_dataloader, valid_dataloader])

    trainer.fit(model)

    # Save the model state for the current iteration
    model_path = f"CAPS_iteration_1"
    torch.save(model.state_dict(), model_path)

    print(f"Finished training iteration {iteration}")

    return model

def main(args):
    argsdict = vars(args)
    print(pprint.pformat(argsdict))

    with open(os.path.join(args.save_dir, "command.txt"), 'w') as f:
        f.write(pprint.pformat(argsdict))
   
    train_data_tt_py, train_data_cs_py, train_data_pd_py, train_data_tt_java, train_data_cs_java, train_data_pd_java = load_tokenize_data(args)
    # train_data_tt_py, train_data_cs_py, train_data_pd_py = load_tokenize_data(args)

    CAPS = CodeT5SmallCAPS() 
        
    # Number of Iteration.
    iteration = 2
    
    model_name = "CAPS"
    for i in range(iteration):
        if args.train_tt:
            print(f"=========== Now Training with Token Type Objective for iteration {i} =============")
            train_dataloader, valid_dataloader = convert_to_loader(train_data_tt_java)
            CAPS = run_pretraining(args, CAPS, train_dataloader, valid_dataloader, i)
            
            train_dataloader, valid_dataloader = convert_to_loader(train_data_tt_py)
            CAPS = run_pretraining(args, CAPS, train_dataloader, valid_dataloader, i+1)
            
        if args.train_cs:
            print(f"=========== Now Training with Code Sememe Objective for iteration {i} =============")
            train_dataloader, valid_dataloader = convert_to_loader(train_data_cs_py)
            CAPS = run_pretraining(args, CAPS, train_dataloader, valid_dataloader, i+1)
            
            train_dataloader, valid_dataloader = convert_to_loader(train_data_cs_java)
            CAPS = run_pretraining(args, CAPS, train_dataloader, valid_dataloader, i+1)
            
        if args.train_pd:
            print(f"=========== Now Training with Program Dependency Objective for iteration {i} =============")
            train_dataloader, valid_dataloader = convert_to_loader(train_data_pd_java)
            CAPS = run_pretraining(args, CAPS, train_dataloader, valid_dataloader, i+1)
            
            train_dataloader, valid_dataloader = convert_to_loader(train_data_pd_py)
            CAPS = run_pretraining(args, CAPS, train_dataloader, valid_dataloader, i+1)
            


    # Save the model with the constructed name
    if args.train_tt:
        model_name += "_TokenType"
    if args.train_cs:
        model_name += "_CodeSememe"
    if args.train_pd:
        model_name += "_PD"
        
    model_save_path = model_name
    CAPS.model.save_pretrained(model_save_path)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Pre-training CAPS on Seq2Seq LM task with Abstract pre-training objectives.")
    parser.add_argument('-tt', '--train-tt', action='store_true', help='Train the model on Token Type Objective')
    parser.add_argument('-cs', '--train-cs', action='store_true', help='Train the model on Code Sememe Objective')
    parser.add_argument('-pd', '--train-pd', action='store_true', help='Train the model on Program Dependency Objective')
    parser.add_argument('--max-source-len', default=1024, type=int)
    parser.add_argument('--max-target-len', default=1024, type=int)
    parser.add_argument('--cache_data_train_data_tt_py', default='cache_data/CAPS_TT', type=str)
    parser.add_argument('--cache_data_train_data_cs_py', default='cache_data/CAPS_CS', type=str)
    parser.add_argument('--cache_data_train_data_pd_py', default='cache_data/CAPS_PD', type=str)
    parser.add_argument('--cache_data_train_data_tt_java', default='cache_data/CAPS_TT_java', type=str)
    parser.add_argument('--cache_data_train_data_cs_java', default='cache_data/CAPS_CS_java', type=str)
    parser.add_argument('--cache_data_train_data_pd_java', default='cache_data/CAPS_PD_java', type=str)
    parser.add_argument('--tokenizer', default='Salesforce/codet5p-220m', type=str)
    parser.add_argument('--epochs', default=1, type=int)
    parser.add_argument('--lr', default=5e-5, type=float)
    parser.add_argument('--lr-warmup-steps', default=200, type=int)
    parser.add_argument('--batch-size-per-replica', default=8, type=int)
    parser.add_argument('--grad-acc-steps', default=4, type=int)
    parser.add_argument('--local_rank', default=-1, type=int)
    parser.add_argument('--deepspeed', default=None, type=str)
    parser.add_argument('--fp16', default=False, action='store_true')
    parser.add_argument('--save-dir', default="trainingDetails/CAPS1", type=str)
    parser.add_argument('--log-freq', default=10, type=int)
    parser.add_argument('--save-freq', default=500, type=int)

    args = parser.parse_args()

    os.makedirs(args.save_dir, exist_ok=True)

    main(args)
